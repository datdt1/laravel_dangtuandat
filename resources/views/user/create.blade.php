@extends('layouts.default')

@section('title')
    {{ isset($detailUser) ? 'Edit' : 'Create' }}
@endsection

@section('content')
<div class="container">
    @if (Session::has('message'))
        <div class="alert alert-danger">{{ Session::get('message') }}</div>
    @endif
    <form action="{{ isset($detailUser) ? route('users.update', $detailUser->id) : route('users.store') }}" method="POST">
        @csrf
        @isset($detailUser)
            @method('PUT')
        @endisset
        <div class="row">
            <h4 class="col-12">{{ isset($detailUser) ? 'Cập nhật' : 'Thêm mới' }}</h4>
            <div class="col-6">
                <label class="@error('mail_address') text-danger @enderror">Địa chỉ mail</label>
                <input type="email" placeholder="Email" name="mail_address" class="form-control @error('mail_address') border border-danger @enderror" value="{{ old('mail_address', isset($detailUser) ? $detailUser->mail_address : '') }}">
                @error('mail_address')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-6">
                <label class="@error('password') text-danger @enderror">Mật khẩu</label>
            <input type="password" name="password" class="form-control @error('password') border border-danger @enderror">
                @error('password')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-6">
                <label class="@error('password_confirmation') text-danger @enderror">Mật khẩu xác nhận</label>
            <input type="password" name="password_confirmation" class="form-control @error('password_confirmation') border border-danger @enderror">
                @error('password_confirmation')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-6">
                <label class="@error('name') text-danger @enderror">Tên</label>
                <input type="text" name="name" class="form-control @error('name') border border-danger @enderror" value="{{ old('name', isset($detailUser) ? $detailUser->name : '') }}">
                @error('name')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-6">
                <label class="@error('address') text-danger @enderror">Địa chỉ</label>
                <input type="text" name="address" class="form-control @error('address') border border-danger @enderror" value="{{ old('address', isset($detailUser) ? $detailUser->address : '') }}">
                @error('address')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-6">
                <label class="@error('phone') text-danger @enderror">Số điện thoại</label>
                <input type="text" name="phone" class="form-control @error('phone') border border-danger @enderror" value="{{ old('phone', isset($detailUser) ? $detailUser->phone : '') }}">
                @error('phone')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-6">
                <label>Vai trò</label>
                <select class="form-control" name="role">
                    <option value="{{ User::ADMIN }}">Quản trị viên</option>
                    <option value="{{ User::STAFF }}">Nhân viên</option>
                </select>
            </div>
            <div class="col-6">
                <label>Lớp</label>
                <select class="form-control" name="class_id">
                    @foreach ($classList as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
            <input type="hidden" value="{{ isset($detailUser) ? $detailUser->id : '' }}" name="id">
            <div class="col-12 pt-2 text-center">
                <input type="submit" value="{{ isset($detailUser) ? 'Cập nhật' : 'Thêm mới' }}" class="btn btn-warning">
            </div>
        </div>
    </form>
    
</div>
@endsection
