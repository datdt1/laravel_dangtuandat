@extends('layouts.default')

@section('title')
    Index
@endsection

@section('content')
    <div class="container-fluid">
        <div class="p-3 mb-3 bg-white rounded mx-3" style="box-shadow: 0 .125rem .25rem rgba(0,0,0,.075)!important;">
            <h4>Tìm kiếm</h4>
            <form action="{{ route('users.index') }}" method="GET">
                <div class="row">
                    <div class="col-2">
                        <input type="text" placeholder="Email" class="form-control" name="txtEmail"
                            value="{{ old('txtEmail') }}">
                    </div>
                    <div class="col-2">
                        <input type="text" placeholder="Name" class="form-control" name="txtName"
                            value="{{ old('txtName') }}">
                    </div>
                    <div class="col-3">
                        <input type="text" placeholder="Address" class="form-control" name="txtAddress"
                            value="{{ old('txtAddress') }}">
                    </div>
                    <div class="col-2">
                        <input type="text" placeholder="Phone" class="form-control" name="txtPhone"
                            value="{{ old('txtPhone') }}">
                    </div>
                    <div class="col-2">
                        <select class="form-control" name="class">
                            @foreach ($classList as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-1">
                        <input type="submit" class="btn btn-success" value="Tìm kiếm">
                    </div>
                </div>
            </form>
        </div>
        <div class="p-3 mb-5 bg-white rounded mx-3" style="box-shadow: 0 .125rem .25rem rgba(0,0,0,.075)!important;">
            <div class="mb-2 row">
                @if(Session::has('message'))
                    <div class="alert  alert-success col-12">{{ Session::get('message') }}</div>
                @endif
                @can('isAdmin')
                    <div class="col-1">
                        <a href="{{ route('users.create') }}" class="btn btn-warning">Thêm mới</a>
                    </div>
                @endcan
            </div>
            <table class="table">
                <thead class="text-center">
                    <th>STT</th>
                    <th>Địa chỉ Mail</th>
                    <th>Tên</th>
                    <th>Địa chỉ</th>
                    <th>Số điện thoại</th>
                    <th>Lớp</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                    @if(count($data) > 0)
                        @foreach($data as $key => $item)
                            <tr>
                                <td>
                                    {{ $key + ($data->currentPage() - 1) * $data->perPage() + 1 }}
                                </td>
                                <td>{{ $item->mail_address }}</td>
                                <td>{{ Helper::toUpperCase($item->name) }}</td>
                                <td>{{ $item->address }}</td>
                                <td>{{ $item->phone }}</td>
                                <td>{{ $item->classroom->name }}</td>
                                <td>
                                    @can('isAdmin')
                                        <a href="{{ route('users.edit', $item->id) }}" class="btn btn-primary">Cập nhật</a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6" class="text-center">Không có dữ liệu</td>
                        </tr>
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6"> {!! $data->links() !!}</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
