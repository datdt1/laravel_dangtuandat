<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Hash;

class User extends Model
{
    use SoftDeletes;

    protected $table = 'users';
    protected $perPage = 20;
    const ADMIN = 1;
    const STAFF = 2;
    protected $fillable = [
        'id', 
        'mail_address', 
        'password', 
        'name', 
        'address', 
        'phone',
        'role',
        'class_id'
    ];

    /**
     * belongTo classroom
     */
    public function classroom()
    {
        return $this->belongsTo('App\Models\Classroom', 'class_id');
    }

    /**
     * Scope a query to get user.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetListUser($query, $searchUser) 
    {
        return $query->where($searchUser);
    }

    /**
     * Search and get data in database
     * 
     * @return array|null
     */
    public function getUsers($request)
    {
        $searchUser = [
            ['mail_address', 'like', '%' . $request->input('txtEmail') . '%'],
            ['name', 'like', '%' . $request->input('txtName') . '%'],
            ['address', 'like', '%' . $request->input('txtAddress') . '%'],
        ];
        if ($request->input('txtPhone') != null) {
            $searchUser[] = ['phone', '=', $request->input('txtPhone')];
        }
        if ($request->input('class') != null) {
            $searchUser[] = ['class_id', '=', $request->input('class')];
        }
        $user = $this->with('classroom')->getListUser($searchUser)->orderby('mail_address')->paginate();
        return $user;
    }

    /**
     * store user
     * 
     * @param $user
     * @return bool
     */
    public function storeUser($user)
    {
        $user['password'] = Hash::make($user['password']);
        $store = $this->create($user);
        if ($store) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * get detail user
     * 
     * @param int $id
     * @return array|null
     */
    public function getDetailUser($id)
    {
        return $this->find($id);
    }

    /**
     * update user
     * 
     * @return bool
     */
    public function updateUser($id, $request)
    {
        $request['password'] != null ? $request['password'] = Hash::make($request['password']) : $request = array_filter($request, 'strlen');
        $update = $this->find($id)->update($request);
        if ($update) {
            return true;
        } else {
            return false;
        }
    }

}
