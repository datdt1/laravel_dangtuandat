<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Classroom extends Model
{
    use SoftDeletes;

    protected $table = 'Classrooms';
    protected $fillable = [
        'name'
    ];

    /**
     * get classroom
     * 
     * @return array|null
     */
    public function getClass() 
    {
        return $this->get();
    }
}
