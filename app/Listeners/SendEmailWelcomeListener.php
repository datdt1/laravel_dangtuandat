<?php

namespace App\Listeners;

use App\Events\RegisteredUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\NewUserNotification;
use Mail;

class SendEmailWelcomeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisteredUser  $event
     * @return void
     */
    public function handle(RegisteredUser $event)
    {
        Mail::to($event->user->mail_address)->send(new NewUserNotification($event->user->name));
    }
}
