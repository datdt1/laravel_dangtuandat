<?php
namespace App\Facade;

use Illuminate\Support\Str;

class Helper
{
    /**
     * @param string $name
     * 
     * @return string
     */
    public static function toUpperCase($name)
    {
        return Str::upper($name);
    }
    
}
