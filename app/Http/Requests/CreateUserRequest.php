<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;


class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $mail = [ 
            'required', 
            'email', 
            'max:100', 
            Request::input('id') != null ? 'unique:users,mail_address,' . Request::get('id') : 'unique:users,mail_address' 
        ];
        $password = [ 
            'max:255', 
            Request::input('id') != null ? 'nullable' : 'required' 
        ];
        $passwordConfirmation = [ 
            Request::input('id') != null ? 'nullable' : 'required', 
            'same:password' 
        ];
        return [
            'mail_address' => $mail,
            'password' => $password,
            'password_confirmation' => $passwordConfirmation,
            'name' => 'required|max:255',
            'address' => 'nullable|max:255',
            'phone' => 'nullable|alpha_num|max:15'
        ];
    }

}
