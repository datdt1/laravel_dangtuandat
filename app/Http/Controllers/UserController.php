<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\CreateUserRequest;
use App\Models\User;
use App\Models\Classroom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use App\Events\RegisteredUser;

class UserController extends Controller
{

    protected $user;
    protected $class;

    /**
    * function construct controller
    *
    * @return void
    */
    public function __construct(User $user, Classroom $class)
    {
        $this->user = $user;
        $this->class = $class;
        $this->middleware('checkclass');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $this->user->getUsers($request);
        $classList = $this->class->getClass();
        $request->flash();
        return view('user.index', compact(['data', 'classList']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isAdmin');
        $classList = $this->class->getClass();
        return view('user.create', compact('classList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $this->authorize('isAdmin');
        $check = $this->user->storeUser($request->all());
        if ($check) {
            Session::flash('message', 'Thêm mới thành công!');
            event(new RegisteredUser($request));
            return redirect()->route('users.index');
        } else {
            Session::flash('message', 'Thêm mới không thành công!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $this->authorize('isAdmin');
        $detailUser = $this->user->getDetailUser($id);
        return view('user.create', compact('detailUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateUserRequest $request, $id)
    {
        $this->authorize('isAdmin');
        $user = $this->user->updateUser($id, $request->all());
        if ($user) {
            Session::flash('message', 'Cập nhật thành công!');
            return redirect()->route('users.index');
        } else {
            Session::flash('message', 'Cập nhật không thành công!');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
