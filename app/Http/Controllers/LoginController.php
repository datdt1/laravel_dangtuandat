<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Auth;

class LoginController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            return redirect()->route('users.index');
        } else {
            return view('auth.login');
        }
    }

    /**
     * Check login
     * 
     * @param array $request
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        $remember = isset($request['remember']) ? true : false;
        if (Auth::attempt($request->only(['mail_address','password']), $remember)) {
            return redirect()->route('users.index');
        } else {
            return redirect()->back()->with('status', 'tài khoản hoặc mật khẩu không chính xác!');
        }
    }

    /**
     * logout
     * 
     * @return \Illuminate\Http\Response
     */
    public function logout() 
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
