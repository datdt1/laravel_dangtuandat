<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckClass
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->class_id < 5) {
            return redirect()->route('home');
        }
        return $next($request);
    }
}
