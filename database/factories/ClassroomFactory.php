<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Classroom;

$factory->define(Classroom::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
