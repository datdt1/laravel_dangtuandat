<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use App\Models\Classroom;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $class = Classroom::all()->pluck('id')->toArray();
    return [
        'mail_address' => $faker->unique()->safeEmail,
        'address' => $faker->address,
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'password' => Hash::make('password'),
        'role' => User::ADMIN,
        'class_id' => $faker->randomElement($class)
    ];
});
